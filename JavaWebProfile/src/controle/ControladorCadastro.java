package controle;

import dominio.Hospede;
import negocio.AplLogin;
import negocio.HospedeMenorDeIdadeException;
import negocio.LoginIncorretoException;
import negocio.NegocioCadastro;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class ControladorCadastro implements Serializable {
	private static final long serialVersionUID = 237290857643825167L;

	@EJB
	private NegocioCadastro aplCadastro;
	
	@EJB
	private AplLogin aplLogin;
	
	private Hospede hospede = new Hospede();
	
	private int idade;

	public Hospede getHospede() {
		return hospede;
	}
	
	@Produces @Logado
	public Hospede getHospedeLogado() {
		if ((hospede == null) || (hospede.getId() == null))
			throw new IllegalStateException("N�o h� um h�spede logado!");
		return hospede;
	}
	
	@Produces @Admin
	public Hospede getHospedeAdmin() {
		if ((hospede == null) || (! hospede.isAdmin()))
			throw new IllegalStateException("O admin n�o esta logado!");
		return hospede;
	}
	
	public int getIdade() {
		return idade;
	}

	public String cadastrar() {
		try {
			aplCadastro.cadastrar(hospede);
		}
		catch (HospedeMenorDeIdadeException e) {
			idade = e.getIdade();
			return "/cadastro/menor.xhtml";
		}
		
		return "/cadastro/sucesso.xhtml";
	}
	
	public boolean isUsuarioLogado() {
		return hospede.getId() != null;
	}
	
	public void login() {
		try {
			hospede = aplLogin.login(hospede.getEmail(), hospede.getSenha());
		}
		catch (LoginIncorretoException e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login incorreto", "N�o foi poss�vel a identifica��o a partir dos dados informados. Por favor, tente novamente.");
			FacesContext ctx = FacesContext.getCurrentInstance();
			ctx.addMessage("formLogin", msg);
		}
	}
	
	public void logout() {
		hospede = new Hospede();
	}
}
