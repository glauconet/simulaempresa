package controle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import negocio.AplAdmin;
import negocio.AplReserva;
import dominio.Cama;
import dominio.Hospede;
import dominio.Quarto;
import dominio.Reserva;

@Named
@ConversationScoped
public class ControladorReserva implements Serializable {
	@EJB
	private AplReserva aplReserva;
	
	@EJB
	private AplAdmin aplAdmin;
	
	@Inject
	private Conversation conversation;
	
	@Inject @Logado
	private Hospede hospede;
	
	private Date checkin;
	
	private Date checkout;
	
	private String[] idsQuartos;
	
	private String[] idsCamas;
	
	private List<Quarto> quartosCadastrados;
	
	private Map<Long, Resultado> mapaQuartos;
	
	private List<Resultado> resultado = new ArrayList<Resultado>();
	
	private List<Cama> camasDisponiveis = new ArrayList<Cama>();
	
	private List<Cama> camasReservadas = new ArrayList<Cama>();

	public Date getCheckin() {
		return checkin;
	}

	public void setCheckin(Date checkin) {
		this.checkin = checkin;
	}

	public Date getCheckout() {
		return checkout;
	}

	public void setCheckout(Date checkout) {
		this.checkout = checkout;
	}

	public List<Quarto> getQuartosCadastrados() {
		if (quartosCadastrados == null) {
			quartosCadastrados = aplAdmin.listar();
		}
		
		return quartosCadastrados;
	}
	
	public String[] getIdsQuartos() {
		return idsQuartos;
	}

	public void setIdsQuartos(String[] idsQuartos) {
		this.idsQuartos = idsQuartos;
	}

	public String[] getIdsCamas() {
		return idsCamas;
	}

	public void setIdsCamas(String[] idsCamas) {
		this.idsCamas = idsCamas;
	}

	public List<Resultado> getResultado() {
		return resultado;
	}
	
	public List<Cama> getCamasDisponiveis() {
		return camasDisponiveis;
	}

	public List<Cama> getCamasReservadas() {
		return camasReservadas;
	}

	public String iniciar() {
		if (!conversation.isTransient()) {
			conversation.end();
		}
		conversation.begin();
		return "/reserva/index.xhtml";
	}

	public String verDisponibilidade() {
		// Obtém as reservas do período e a listagem de camas.
		List<Reserva> reservasDoPeriodo = aplReserva.listarReservasAtivas(checkin, checkout);
		List<Cama> camas = aplReserva.listarCamas();
		
		// Remove as camas que não estão disponíveis.
		for (Reserva reserva : reservasDoPeriodo)
			for (Cama cama : reserva.getCamas())
				camas.remove(cama);
		
		// Monta um mapa de camas disponíveis, indexadas por quarto.
		mapaQuartos = new HashMap<Long, Resultado>();
		for (Cama cama : camas) {
			Quarto quarto = cama.getQuarto();
			Resultado result = mapaQuartos.get(quarto.getId());
			if (result == null) {
				result = new Resultado(quarto, cama);
				mapaQuartos.put(quarto.getId(), result);
			}
			else result.adicionar(cama);
		}
		
		// Coloca as camas disponíveis em um objeto que representa o resultado da pesquisa.
		resultado.addAll(mapaQuartos.values());
		return "/reserva/pesquisa.xhtml";
	}
	
	public String prosseguir() {
		for (String sId : idsQuartos) {
			try {
				Long id = Long.parseLong(sId);
				Resultado result = mapaQuartos.get(id);
				if (result != null)
					for (Cama cama : result.camasDisponiveis)
						camasDisponiveis.add(cama);
			}
			catch (NumberFormatException e) {
				System.err.println("Erro ao tentar converter \"" + sId + "\" para Long: " + e.getMessage());
			}
		}
		
		return "/reserva/camas.xhtml";
	}
	
	public String reservar() {
		try {
			for (String sId : idsCamas) {
				Long id = Long.parseLong(sId);
				Cama cama = aplReserva.consultarCama(id);
				if (cama != null)
					camasReservadas.add(cama);
			}
			aplReserva.reservar(hospede, checkin, checkout, camasReservadas);
		}
		catch (NumberFormatException e) {
			System.err.println("Vetor de IDs das camas inválido -- " + Arrays.toString(idsCamas) + ". Erro ao converter um dos valores para Long: " + e.getMessage());
			return "/reserva/camas.xhtml";
		}
		
		conversation.end();
		return "/reserva/fim.xhtml";
	}
	
	public class Resultado {
		private Quarto quarto;
		
		private Set<Cama> camasDisponiveis = new HashSet<Cama>();
		
		Resultado(Quarto quarto, Cama cama) {
			this.quarto = quarto;
			camasDisponiveis.add(cama);
		}
		
		public Long getIdQuarto() {
			return quarto.getId();
		}
		
		public int getNumQuarto() {
			return quarto.getNumero();
		}
		
		public int getNumCamasTotal() {
			return quarto.getNumCamas();
		}
		
		public int getNumCamasDisponiveis() {
			return camasDisponiveis.size();
		}
		
		public void adicionar(Cama cama) {
			camasDisponiveis.add(cama);
		}
	}
}
