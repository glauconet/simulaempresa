package controle;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


import negocio.AplAdmin;
import dominio.Cama;
import dominio.Hospede;
import dominio.Quarto;

@Named
@RequestScoped
public class ControladorAdmin implements Serializable {
	private static final long serialVersionUID = 5199627012561256472L;

	@EJB
	private AplAdmin aplAdmin;
	
	@SuppressWarnings("unused")
	@Inject @Admin
	private Hospede admin;
	
	private Quarto quarto = new Quarto();
	
	private List<Quarto> quartosCadastrados;
	
	private Cama cama = new Cama();
	
	private Long idQuarto;
	
	public Long getIdQuarto() {
		return idQuarto;
	}

	public void setIdQuarto(Long idQuarto) {
		this.idQuarto = idQuarto;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public Cama getCama() {
		return cama;
	}

	public List<Quarto> getQuartosCadastrados() {
		if (quartosCadastrados == null) {
			quartosCadastrados = aplAdmin.listar();
		}
		
		return quartosCadastrados;
	}

	public void cadastrarQuarto() {
		aplAdmin.cadastrar(quarto);
		quartosCadastrados.add(quarto);
		quarto = new Quarto();
	}
	
	public void cadastrarCama() {
		if (idQuarto != null) {
			cama.setQuarto(aplAdmin.consultarQuarto(idQuarto));
			aplAdmin.cadastrar(cama);
			cama = new Cama();
			quartosCadastrados = null;
		}
	}
}
