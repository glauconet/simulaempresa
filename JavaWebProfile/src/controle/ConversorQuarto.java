package controle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import negocio.AplAdmin;
import dominio.Quarto;

public class ConversorQuarto implements Converter {
	private AplAdmin aplAdmin;
	
	public ConversorQuarto(AplAdmin aplAdmin) {
		this.aplAdmin = aplAdmin;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Quarto quarto = null;
		if ((value != null) && (value.trim().length() > 0)) {
			try {
				Long id = Long.valueOf(value);
				System.out.println("getAsObject() -- id = " + id);
				quarto = aplAdmin.consultarQuarto(id);
				System.out.println("getAsObject() -- quarto = " + quarto);
			}
			catch (NumberFormatException e) {
				System.err.println("Erro de conversão: " + e);
				return null;
			}
			catch (Exception e) {
				System.err.println("Erro desconhecido: " + e);
				return null;
			}
		}
		
		return quarto;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if ((value != null) && (value instanceof Quarto)) {
			Quarto quarto = (Quarto) value;
			return String.valueOf(quarto.getId());
		}
		
		return null;
	}

}
