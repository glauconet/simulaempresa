package negocio;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dominio.Cama;
import dominio.Hospede;
import dominio.Reserva;
import dominio.Reserva_;

@Stateless
@LocalBean
public class AplReserva implements Serializable {
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Reserva> listarReservasAtivas(Date dataCheckin, Date dataCheckout) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Reserva> cq = cb.createQuery(Reserva.class);
		Root<Reserva> root = cq.from(Reserva.class);
		cq.select(root);
		cq.where(cb.or(
				cb.between(root.get(Reserva_.dataInicio), dataCheckin, dataCheckout),
				cb.between(root.get(Reserva_.dataFim), dataCheckin, dataCheckout)));
		
		return entityManager.createQuery(cq).getResultList();
	}
	
	public List<Cama> listarCamas() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Cama> cq = cb.createQuery(Cama.class);
		Root<Cama> root = cq.from(Cama.class);
		cq.select(root);
		return entityManager.createQuery(cq).getResultList();
	}
	
	public Cama consultarCama(Long id) {
		return entityManager.find(Cama.class, id);
	}
	
	public void reservar(Hospede hospede, Date dataCheckin, Date dataCheckout, List<Cama> camas) {
		for (Cama cama : camas)
			entityManager.merge(cama);
		
		Reserva reserva = new Reserva();
		reserva.setHospede(hospede);
		reserva.setDataInicio(dataCheckin);
		reserva.setDataFim(dataCheckout);
		Set<Cama> camasReservadas = new HashSet<Cama>();
		camasReservadas.addAll(camas);
		reserva.setCamas(camasReservadas);
		entityManager.persist(reserva);
	}
}
