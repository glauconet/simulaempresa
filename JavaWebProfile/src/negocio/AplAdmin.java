package negocio;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dominio.Cama;
import dominio.Quarto;

@Stateless
@LocalBean
public class AplAdmin implements Serializable {
	private static final long serialVersionUID = 258298873654951938L;

	@PersistenceContext
	private EntityManager entityManager;

	public List<Quarto> listar() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Quarto> cq = cb.createQuery(Quarto.class);
		Root<Quarto> root = cq.from(Quarto.class);
		cq.select(root);
		return entityManager.createQuery(cq).getResultList();
	}
	
	public Quarto consultarQuarto(Long id) {
		return entityManager.find(Quarto.class, id);
	}
	
	public void cadastrar(Quarto quarto) {
		entityManager.persist(quarto);
	}
	
	public void cadastrar(Cama cama) {
		Quarto quarto = cama.getQuarto();
		Set<Cama> camas = quarto.getCamas();
		if (camas == null) {
			camas = new HashSet<Cama>();
			quarto.setCamas(camas);
		}
		camas.add(cama);
		entityManager.merge(quarto);
	}
}
