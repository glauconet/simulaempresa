package negocio;

public class HospedeMenorDeIdadeException extends Exception {
	private static final long serialVersionUID = -6048658767002019085L;

	private int idade;

	public HospedeMenorDeIdadeException(int idade) {
		this.idade = idade;
	}
	
	public int getIdade() {
		return idade;
	}
}
