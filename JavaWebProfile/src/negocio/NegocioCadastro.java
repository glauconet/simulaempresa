package negocio;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import dominio.Hospede;

@Stateless
@LocalBean
public class NegocioCadastro implements Serializable {
	private static final long serialVersionUID = -2203559511425200552L;

	@PersistenceContext
	private EntityManager entityManager;
	
	public void cadastrar(Hospede hospede) throws HospedeMenorDeIdadeException {
		
		int idade = calcularIdade(hospede.getDataNascimento());
		if (idade < 18) throw new HospedeMenorDeIdadeException(idade);
		entityManager.persist(hospede);
	}
	
	private static int calcularIdade(Date dataNascimento) {
		if (dataNascimento == null) return 0;
		
		Calendar nasc = Calendar.getInstance();
		nasc.setTime(dataNascimento);
		Calendar hoje = Calendar.getInstance();
		hoje.setTime(new Date(System.currentTimeMillis()));
		
		int idade = hoje.get(Calendar.YEAR) - nasc.get(Calendar.YEAR);
		nasc.add(Calendar.YEAR, idade);
		if (nasc.after(hoje)) idade--;
		return idade;
	}
}
