package negocio;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dominio.Hospede;
import dominio.Hospede_;

@Stateless
@LocalBean
public class AplLogin implements Serializable {
	private static final long serialVersionUID = 4780685528191149745L;

	@PersistenceContext
	private EntityManager entityManager;
	
	public Hospede login(String email, String senha) throws LoginIncorretoException {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Hospede> cq = cb.createQuery(Hospede.class);
		Root<Hospede> root = cq.from(Hospede.class);
		cq.where(cb.equal(root.get(Hospede_.email), email));
		TypedQuery<Hospede> query = entityManager.createQuery(cq);
		
		// Lança a exceção se não há hóspedes com o e-mail indicado.
		Hospede hospede = null;
		try {
			hospede = query.getSingleResult();
		}
		catch (Exception e) {
			throw new LoginIncorretoException();
		}
		
		// Lança a exceção se a senha não confere.
		if ((senha == null) || (! senha.equals(hospede.getSenha())))
			throw new LoginIncorretoException();
		
		// Se passou as duas verificações, login correto. Retorna o hóspede.
		return hospede;
	}
}
