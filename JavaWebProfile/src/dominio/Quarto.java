package dominio;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Quarto implements Serializable {
	private static final long serialVersionUID = 4844124395404456540L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private int numero;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "quarto", fetch = FetchType.EAGER)
	private Set<Cama> camas;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Set<Cama> getCamas() {
		return camas;
	}
	public void setCamas(Set<Cama> camas) {
		this.camas = camas;
	}
	public int getNumCamas() {
		if (camas == null) return 0;
		return camas.size();
	}
}
