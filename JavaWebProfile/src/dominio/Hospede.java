package dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Hospede implements Serializable {
	private static final long serialVersionUID = -7687806765154165017L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	private String email;
	
	private String senha;
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public boolean isAdmin() {
		// Para simplificar, o primeiro usuário cadastrado é automaticamente o administrador.
		// Caso utilize um banco de dados que não atribua o valor 1 ao ID do primeiro objeto cadastrado em uma
		// tabela, altere o valor abaixo para o ID do administrador e faça o redeploy da aplicação.
		return (id != null) && (id == 1);
	}
}
