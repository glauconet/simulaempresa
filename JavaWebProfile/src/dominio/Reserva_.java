package dominio;

import java.util.Date;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Reserva.class)
public class Reserva_ {
	public static volatile SingularAttribute<Reserva, Long> id;
	public static volatile SingularAttribute<Reserva, Hospede> hospede;
	public static volatile SetAttribute<Reserva, Cama> camas;
	public static volatile SingularAttribute<Reserva, Date> dataInicio;
	public static volatile SingularAttribute<Reserva, Date> dataFim;
}
