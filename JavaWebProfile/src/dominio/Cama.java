package dominio;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Cama implements Serializable {
	private static final long serialVersionUID = 8591026891002820776L;
	private static final NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Quarto quarto;
	
	private int numero;
	
	private double precoNoite;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Quarto getQuarto() {
		return quarto;
	}
	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public double getPrecoNoite() {
		return precoNoite;
	}
	public void setPrecoNoite(double precoNoite) {
		this.precoNoite = precoNoite;
	}
	public String getPrecoFormatado() {
		return nf.format(precoNoite); 
	}
	
	@Override
	public boolean equals(Object obj) {
		return (id != null) && (obj != null) && (obj instanceof Cama) && (id.equals(((Cama) obj).id));
	}
}
