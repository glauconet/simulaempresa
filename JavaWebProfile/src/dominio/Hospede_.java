package dominio;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Hospede.class)
public class Hospede_ {
	public static volatile SingularAttribute<Hospede, Long> id;
	public static volatile SingularAttribute<Hospede, String> nome;
	public static volatile SingularAttribute<Hospede, String> email;
	public static volatile SingularAttribute<Hospede, String> senha;
	public static volatile SingularAttribute<Hospede, Date> dataNascimento;
}
